#!/bin/bash

# Update package list
sudo apt-get update

# Install Python 3
sudo apt-get install -y python3

# Install software-properties-common
sudo apt install -y software-properties-common

# Add Ansible repository
sudo add-apt-repository --yes --update ppa:ansible/ansible

# Install Ansible
sudo apt install -y ansible

# Create Ansible user
sudo useradd -m -s /bin/bash ansible

# Set password for Ansible user
echo 'ansible:password123' | sudo chpasswd

# Add Ansible user to sudo group
sudo usermod -aG sudo ansible

# Create SSH directory for Ansible user
sudo mkdir /home/ansible/.ssh

# Set permissions for SSH directory
sudo chmod 700 /home/ansible/.ssh

# Copy authorized_keys from ubuntu to ansible
sudo cp /home/ubuntu/.ssh/authorized_keys /home/ansible/.ssh/authorized_keys

# Set permissions for authorized_keys
sudo chmod 600 /home/ansible/.ssh/authorized_keys

# Adjust ownership of .ssh directory
sudo chown -R ansible:ansible /home/ansible/.ssh

# Install Node.js version 20 without using NVM
curl -sL https://deb.nodesource.com/setup_20.x | sudo -E bash -
sudo apt-get install -y nodejs

# Enable public key authentication in SSH server config
sudo sed -i 's/#PubkeyAuthentication yes/PubkeyAuthentication yes/' /etc/ssh/sshd_config

# Allow passwordless sudo for the Ansible user
echo 'ansible ALL=(ALL) NOPASSWD: ALL' | sudo tee -a /etc/sudoers

# Restart SSH service
sudo service ssh restart
