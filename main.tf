# main.tf

resource "aws_instance" "ubuntu_instance" {
  count         = 2
  ami           = "ami-0287a05f0ef0e9d9a" # Ubuntu 20.04 LTS AMI ID
  instance_type = "t2.micro"
  key_name      = "Terraform" # Set your key pair name
  vpc_security_group_ids = [aws_security_group.ubuntu_sg.id]
  tags = {
    Name = "ubuntu-instance-${count.index + 1}"
  }
  provisioner "file" {
    source      = "ansible.sh"
    destination = "/home/ubuntu/ansible.sh"  # Adjust the destination as needed
  }
  # Execute the configure_jenkins.sh script on the instance
  provisioner "remote-exec" {
    inline = [
      "chmod +x /home/ubuntu/ansible.sh",
      "/home/ubuntu/ansible.sh",
    ]
  }
  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("Terraform.pem") # Set the path to your private key
    host        = self.public_ip
  }
}

# ansible_inventory.tf

resource "null_resource" "ansible_inventory" {
  depends_on = [aws_instance.ubuntu_instance]

  provisioner "local-exec" {
    command = <<EOF
      echo "[ubuntu_instances]" > ansible_inventory.ini
      echo "${join("\n", [for instance in aws_instance.ubuntu_instance : "${instance.public_ip} ansible_ssh_private_key_file=Terraform.pem"])}" >> ansible_inventory.ini
    EOF
  }
}

# output.tf

output "public_ips" {
  value = aws_instance.ubuntu_instance[*].public_ip
} 